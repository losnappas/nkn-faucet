const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express')
const hcaptcha = require('./express-hcaptcha');
const path = require('path')
const nknWallet = require('nkn-wallet');

const PORT = process.env.PORT || 5000
// your hcaptcha secret key
const SECRET = process.env.HCAPTCHA_SECRET_KEY;
const NKN_PRIVATE_KEY = process.env.NKN_PRIVATE_KEY;
const TRANSFER_AMOUNT = 1e-5;
const FEE = 0; // 1e-8; // Absolutely no difference.

const wallet = nknWallet.restoreWalletBySeed( NKN_PRIVATE_KEY, 'lol' );

const app = express();

app.use(cors())
	.use(bodyParser.urlencoded({extended: false})) // required by express-hcaptcha)
	.use(express.static(path.join(__dirname, 'public')))
	.set('views', path.join(__dirname, 'views'))
	.set('view engine', 'ejs')
	.get('/', (req, res) => res.render('pages/index', { query: req.query }))
	// validate the token and proceed to the route when token is valid
	// the middleware also sets the req.hcaptcha to what ever the verify call returns
	.post('/verify', hcaptcha.middleware.validate(SECRET), (req, res) => {
		const toAddress = req.body.nkn;

		wallet.transferTo(toAddress, TRANSFER_AMOUNT, { fee: FEE })
		.then(tx => res.redirect('/?tx=' + encodeURIComponent(tx)))
		.catch(err => res.redirect('/?err=' + encodeURIComponent(err.msg)))
	})
	.listen(PORT, () => console.log(`Listening on ${ PORT }`))
